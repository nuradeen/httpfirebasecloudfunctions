const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

exports.adduser = functions.https.onRequest((request,response)=>{

    const email = request.query.email;
    const password = request.query.pass;
    const position = request.query.position;
    const phone = request.query.phone;
    const firstname = request.query.firstname;
    const lastname = request.query.lastname;
    

    admin.auth().createUser({
        email: email,
        emailVerified: false,
        password: password,
        disabled: false
    })
    .then(function(record){
        // response.send({"uid":record.uid});

        var userobject = {
            "FirstName" : firstname,
            "Lastname" : lastname,
            "Position" : position,
            "Phoneumber" : phone,
            "Image" : "",
            "ImageCover" : "",
            "Usertype" : "0"
        }

        admin.database().ref('Users').child(record.uid).set(userobject);

       // console.log("Successfully created new user:", record);
       // response.send({"error":0,"message":"Successfully , Create new user.","data": record.uid});
        return 1;
    })
    .catch(function(error){
        // response.send("Error creating new user");
        console.log("Error creating new user:", error);
        response.send({"error":1,"message":"Can't create user , please check your email."});
        return 1;
    });
    return 1;
});
