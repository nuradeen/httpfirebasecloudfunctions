/* eslint-disable promise/always-return */
/* eslint-disable prefer-arrow-callback */
// Initialize Firebase connect config
//sdk config
  var config = {
    apiKey: "AIzaSyD0qOcSTfWVvspg2sftCEyxxOZBmdaNdZo",
    authDomain: "fir-a51cb.firebaseapp.com",
    databaseURL: "http://fir-a51cb.firebaseio.com",
    projectId: "fir-a51cb",
    storageBucket: "fir-a51cb.appspot.com"
    // ,messagingSenderId: "786149799293"
    
  };
  firebase.initializeApp(config);

//   const functions = require('firebase-functions');
// const admin = require('firebase-admin');
  // var firebase = require('firebase');
  // var firebaseui = require('firebaseui');
 
  //ref message collection connect RTDB
  var messagesRef = firebase.database().ref('users');
  var user = firebase.auth();


 //var database = firebase.database();

  //form submit get form id='contactForm' and get button id='submit'
  document.getElementById('contactForm').addEventListener('submit', SubmitForm);
  
  function SubmitForm(e)
  {
    e.preventDefault();
    //initialization & get id from input
    var name = getInputVal('name');
    var company = getInputVal('company');
    var email = getInputVal('email');
    var phone = getInputVal('phone');
    var message = getInputVal('message');
    
    //Save message
    saveMessage(name, company, email, phone, message);
    
    //show alert if success!
    document.querySelector('.alert').style.display = 'block';
    
    //hide alert after 3 secs.
    // eslint-disable-next-line prefer-arrow-callback
    setTimeout(function()
    {
      document.querySelector('.alert').style.display = 'none';
    },3000);
    
    //clear form after submit
    document.getElementById('contactForm').reset();
  }

  //get form value by id
  function getInputVal(id)
  {
    return document.getElementById(id).value;
  }

  //save message to firebase
  function saveMessage(name, company, email, phone, message)
  {
    var newMessageRef = messagesRef.push();
      newMessageRef.set(
        {
          name: name,
          company: company,
          email: email,
          phone: phone,
          message: message
        })
        console.log('insert success!');
  }
  



  
  //query database.reference
  var firebaseRef = firebase.database().ref('users');
  var query = firebase.database().ref('Admin');
  var query2 = firebase.database().ref('test');
  // eslint-disable-next-line promise/catch-or-return
  // eslint-disable-next-line prefer-arrow-callback
  // eslint-disable-next-line promise/always-return
  // eslint-disable-next-line promise/catch-or-return
  firebaseRef.once("value").then(function(dataSnapshot)
  {
    console.log(dataSnapshot.val());
    // console.log('query success!');
  });

//   var query = firebase.database().ref("books").orderByChild("author").equalTo("Colson Whitehead");
// query.on("value", function(snapshot) {
//   snapshot.forEach(function(bookSnapshot) {
//     console.log(bookSnapshot.key+": "+bookSnapshot.val());
//   });
// })
  
  //create node 'admin' when click button สร้างโหนดชื่อ "Admin" จาก root
    // function saveOnclick(){
    //      var firebaseRef = firebase.database().ref();
    //      firebaseRef.child("Admin").set("root");
    // }
  
  // when click button is add child "name" on "Admin" node สร้าง node "name" ภายใต้ node "Admin" และภายใน node "name" มี attribute fname,lname 
  function saveOnclick()
  {
    var firebaseRef = firebase.database().ref("Admin");
    firebaseRef.child("name/fname").set("root");
    firebaseRef.child("name/lname").set("firename");
            
  }
  
  //Delete stringify( , replacer?: any, space?: any)atic
  function delOnclick()
  {
    var firebaseRef = firebase.database().ref("Admin/name/lname");
    // eslint-disable-next-line promise/catch-or-return
    firebaseRef.remove().then(function()
    {
      console.log("remove success!");
    }).catch(function(error) 
    {
      console.log('Remove failed!' + error.message);
    })
  }

  //update data
  function updateData(){
    var firebaseRef = firebase.database().ref("Admin/name");
    firebaseRef.update({
      fname: 'test update'
    });
    console.log('updated success!');

  }
  

/////////////////////get element/////////////////////////////////
 const txtEmail = document.getElementById('txtEmail');
 const txtPassword = document.getElementById('txtPassword');
 const btnLogin = document.getElementById('btnLogin');
 const btnSignUp = document.getElementById('btnSignUp');
 const btnLogout = document.getElementById('btnLogout');

 //add btn login
 btnLogin.addEventListener('click', e => {
   //get email and password
   const email = txtEmail.value;
   const pass = txtPassword.value;
   const auth = firebase.auth();
   
   //sign in
   const promise = auth.signInWithEmailAndPassword(email, pass);
     if (pass.length < 6) {
     console.log('รหัสผ่านน้อยกว่า 6')
     
   } else {
     console.log('log in success!');
       promise.catch(e => console.log(e.message));
     
   }
 });

 //logout event
 btnLogout.addEventListener('click', e => {
   if (btnLogout) {
     firebase.auth().signOut();
     console.log('logout success!');
   }
   

 });
 
 //add btn Signup
 btnSignUp.addEventListener('click', e =>{
   //get email and password
   const email = txtEmail.value;
   const pass = txtPassword.value;
   const auth = firebase.auth();
   //sign in
   const promise = auth.createUserWithEmailAndPassword(email, pass);
   if (!email || !pass) {
     console.log('กรุณาป้อนข้อมูลให้ครบ')
   }
   else if(pass.length<6){
     console.log('รหัสผ่านน้อยกว่า 6');
   }
   else{
    console.log('Sign up สำเร็จ!');
    promise.catch(e => console.log(e.message));
   }

 });

 //add a realtime listener
 firebase.auth().onAuthStateChanged(function(firebaseUser) {
  if (firebaseUser) {
    // User is signed in.
    console.log(firebaseUser);
    console.log('sign in สำเร็จ!');
    btnLogout.classList.remove('hide');
  } else {
    // No user is signed in.
    console.log('ยังไม่ได้ Sign in');
    btnLogout.classList.add('hide');
  }
});



// //google login authentication
// var provider = new firebase.auth.GoogleAuthProvider();
// function googleSignIn()
// {
  
//   firebase.auth().signInWithPopup(provider).then(function(result){
//     var token = result.credential.accessToken;
//     var user = result.user;
//     console.log(token)
//     console.log(user)
//     console.log("success Google Account linked!")
//   }).catch(function(error){
//     var errorCode = error.code;
//   var errorMessage = error.message;

//     console.log(errorCode)
//     console.log(errorMessage)
//     console.log("failed!")
//   })
// }
$(document).ready(function(){
  $("uploadButton").hide();
  document.getElementById("upload").addEventListener('change', handleFileSelect, false);  
});

//upload file
$("#file").on("change", function(event){
  selectedFile = event.target.files[0];
  $("#uploadButton").show();
});

function uploadFile()
{
  var filename = selectedFile.name;
  var storageRef = firebase.storage().ref('/images/' + filename);
  var uploadTask = storageRef.put(selectedFile);

  uploadTask.on('state_changed', function(snapshot)
  {
    // Observe state change events such as progress, pause, and resume
    // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded\
  }, function(error) {
      // Handle unsuccessful uploads
  }, function() {
  // Handle successful uploads on complete
  // For instance, get the download URL: https://firebasestorage.googleapis.com/...
  var downloadURL = uploadTask.snapshot.getDownloadURL;
  //uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
    firebase.database().ref('users/');
    console.log(downloadURL);
  });
}